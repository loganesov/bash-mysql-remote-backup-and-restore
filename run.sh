#!/bin/bash

source './app/enviroment.env'
source './app/functions.sh'

checkingAppVersion

clear

if [[ -z $remoteSSHHost ]]; then
  printHostError
else
  if [[ -z ${remoteListDB[@]} ]]; then
    fetchRemoteDBList

    if [[ $isManualSelectionDB -eq '1' ]]; then
      showRemoteDBListFromFile
    fi
  fi

  clear

  backup

  clear

  if [[ $showBackupInfo -eq '1' && $isCronMode -eq '0' ]]; then
    backup_info
  fi
fi

printTotalScriptExecutionTime $startTimeCommon
startScriptDateTime $startTimeScript
doneScriptDateTime

osMessage "Total script execution time: $(displaytime $(($(date +%s) - $startTimeCommon))) \n\nSTART: $currentDate $startTimeScript\nDONE: $currentDate $(date +"%H:%M:%S")"

exit
