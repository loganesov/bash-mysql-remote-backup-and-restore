
remoteListDBForMenu=()
remoteListDBForMenuSelect=()
remoteListDBFilterString=''
remoteAdditionalQuery=''

function initListDBFilterAsString {
if [[ -n ${remoteListDBFilter[@]} ]]; then
	for i in ${!remoteListDBFilter[@]}; do
		if [[ -z $remoteListDBFilterString ]]; then
			remoteListDBFilterString="${remoteListDBFilter[$i]}"
		else			
			remoteListDBFilterString+="\|${remoteListDBFilter[$i]}"
		fi
	done
fi
}

function fetchRemoteDBList {
	
	initListDBFilterAsString

	if [[ -n $remoteListDBFilterString ]]; then
		remoteAdditionalQuery=' | grep "'$remoteListDBFilterString'"'
	else
		remoteAdditionalQuery=' | tr -d "\+" | tr -s [:space:] "\n"'
	fi

	printRemoteMsg 'BUILD TEMP FILE WITH DATABASE LIST'
	ssh $remoteSSHHost 'cd '$remotePath' && mysqlshow -u'$remoteMysqlUser' -p'$remoteMysqlPassword ' | sort | tr -d "|"'$remoteAdditionalQuery' > '$remoteFilePathDBList " && sed -i '/--------------------/d'" $remoteFilePathDBList " && sed -i '/Databases/d'" $remoteFilePathDBList

	printLocalMsg 'FETCH TEMP FILE WITH DATABASE LIST'
	rsync -avzP $remoteSSHHost':'$remoteFilePathDBList $localPath'/'$remoteFileDBListAsLocal

	printRemoteMsg 'REMOVE TEMP FILE WITH DATABASE LIST'
	ssh $remoteSSHHost ' rm '$remoteFilePathDBList

	fillListRemoteDB
}

function fillListRemoteDB {
	count=0

	while read line; do 
		if [[ $isManualSelectionDB -eq '1' ]]; then
			count=$[count+1]
			remoteListDBForMenu="${remoteListDBForMenu} ${count} $line off "
			remoteListDBForMenuSelect+=($line)
		else
			remoteListDB+=($line)
		fi		
	done < $localPath'/'$remoteFileDBListAsLocal
}

function showRemoteDBListFromFile {

  if [ ! "$(command -v dialog)" ]; then

    PS3="Not found Program: Dialog, install ? "

      select optionUpdateBackupSystem in yes no; do
        case $optionUpdateBackupSystem in
        'yes')
          sudo apt update
          sudo apt install dialog
          break;;
        'no')
          emptyRemoteListDB
          break;;
        *)
          echo "Invalid selected option $REPLY";;
        esac
        sleep 1
      done
  fi

	cmd=(dialog --separate-output --checklist "Press the *SPACEBAR* to select the databases you want to download:" 22 76 16 )
	
	options=(${remoteListDBForMenu})
	
	choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)

	for choice in $choices; do
		remoteListDB+=(${remoteListDBForMenuSelect[$choice - 1]})
	done

	clear

	if [[ -z ${!remoteListDB[@]} ]]; then
		emptyRemoteListDB
	fi
}
