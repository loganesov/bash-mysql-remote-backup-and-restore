function printRemoteMsg {
  if [[ $isCronMode -eq '0' ]]; then
    echo $(tput bold)$(tput setab 4)'REMOTE:    '$(tput sgr0) $(tput setaf 6)' --- '$1': *** '$(tput sgr0) $(tput bold)$(tput setaf 7)$2$(tput sgr0)
    echo
  fi
}

function printLocalMsg {
  if [[ $isCronMode -eq '0' ]]; then
    echo $(tput bold)$(tput setab 2)'LOCAL:     '$(tput sgr0) $(tput setaf 6)' --- '$1': *** '$(tput sgr0) $(tput bold)$(tput setaf 7)$2$(tput sgr0)
    echo
  fi
}

function printContainerMsg {
  if [[ $isCronMode -eq '0' ]]; then
    echo $(tput bold)$(tput setab 5)'CONTAINER: '$(tput sgr0) $(tput setaf 6)' --- '$1': *** '$(tput sgr0) $(tput bold)$(tput setaf 7)$2$(tput sgr0)
    echo
  fi
}

function printOneLine {
  if [[ $isCronMode -eq '0' ]]; then
    echo $(tput bold)$(tput setab 3)$1':  '$(tput sgr0) $(tput bold)$(tput setaf 7) $2 $(tput sgr0)
  fi
}

function printMsg {
  if [[ $isCronMode -eq '0' ]]; then
    echo $(tput bold)$(tput setab 3)$1':  '$(tput sgr0) $(tput bold)$(tput setaf 7) $2 $(tput sgr0)
    echo
  fi
}

function printOneLineSimple {
  if [[ $isCronMode -eq '0' ]]; then
    echo $(tput bold)$(tput setaf 7)$1':  '$(tput sgr0) $(tput setaf 7) $2 $(tput sgr0)
  fi
}

function startScriptDateTime {
  echo $(tput bold)$(tput setaf 2)'START'$(tput sgr0) $currentDate' '$1
}

function doneScriptDateTime {
  echo $(tput bold)$(tput setaf 2)'DONE'$(tput sgr0) $currentDate' '$(date +"%H:%M:%S")
}

function emptyRemoteListDB {
  printLocalMsg "REMOTE DB LIST IS EMPTY"
  doneScriptDateTime
  exit
}

function printHostError {
  echo 'Set parameter '$(tput bold)$(tput setaf 1)'remoteSSHHost'$(tput sgr0)' in '$(tput bold)$(tput setaf 2)'config.env'$(tput sgr0)
  echo '------------------------------------------'
  echo 'Make sure all parameters are filled in '$(tput bold)$(tput setaf 4)$PWD$(tput sgr0)$(tput bold)$(tput setaf 2)'/config.env'$(tput sgr0)
  echo '------------------------------------------'
}

function printTotalScriptExecutionTime {
  echo
  echo "Total script execution time:" $(tput bold) $(displaytime $(($(date +%s) - $1))) $(tput sgr0)
  echo
}

function osMessage {
  if [ -n "$(command -v kdialog)" ]; then
    kdialog "$appName" --title "$1"
  elif [ -n "$(command -v notify-send)" ]; then
    notify-send --expire-time=30000 "$appName" "$1"
  elif [ -n "$(command -v zenity)" ]; then
    zenity --notification --text="$1"
  elif [ -n "$(command -v xmessage)" ]; then
    xmessage -center "$appName: $1"
  else
    printf "%s\n%s\n" "$appName" "$1"
  fi
}
