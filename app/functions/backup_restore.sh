function backup_restore {
  for db in ${localListDB[@]}; do
    if [[ $db == "$1" ]]; then
      if [[ $isDockerSystem -eq '0' ]]; then
        backup_restore_for_mysql_in_system $1
      else
        backup_restore_for_docker_container $1
      fi
    fi
  done
}


function backup_restore_for_mysql_in_system {
  printLocalMsg 'UNCOMPRESSED FILE ' $1'__'$currentDate'.sql.gz'
  gunzip -v -c $localPath'/'$1'__'$currentDate'.sql.gz' > $localTempPath'/'$1'__'$currentDate'.sql'

  printLocalMsg 'DROP TABLES IN DATABASE' $1
  mysqldump --verbose --force -u$localMysqlUser -p$localMysqlPassword --add-drop-table --no-data $1 | grep ^DROP | mysql --verbose --force -u$localMysqlUser -p$localMysqlPassword $1

  printLocalMsg 'RESTORE DATABASE' $1'__'$currentDate'.sql'

  if [[ $isNeedCreateLocalMissingDB -eq '1' ]]; then
    printLocalMsg 'CREATED DATABASE' $1
    mysql --verbose -u$localMysqlUser -p$localMysqlPassword -e 'CREATE DATABASE IF NOT EXISTS `'$1'` CHARACTER SET utf8 COLLATE utf8_general_ci'
  fi

  mysql --verbose -u$localMysqlUser -p$localMysqlPassword --force -w $1 < $localTempPath'/'$1'__'$currentDate'.sql'

  printLocalMsg 'DELETE UNCOMPRESSED FILE ' $1'__'$currentDate'.sql'
  rm $localTempPath'/'$1'__'$currentDate'.sql'
}


function backup_restore_for_docker_container {
  containerName=$dockerContainerPrefix$1$dockerContainerPostfix
  containerDB=$dockerDBPrefix$1$dockerDBPostfix

  printContainerMsg "COPY FILE $1__$currentDate.sql.gz TO CONTAINER $containerName"
  docker cp "$PWD/backup/$1__$currentDate.sql.gz" "$containerName:$dockerTempPath"
  sleep 1

  printContainerMsg "UNCOMPRESSED FILE $1__$currentDate.sql.gz"
  docker exec $containerName bash -c "gunzip -v $dockerTempPath/$1__$currentDate.sql.gz"
  sleep 1

  printContainerMsg "DROP TABLES FOR DATABASE $1 IN CONTAINER $containerName"
  docker exec $containerName bash -c "echo 'SET FOREIGN_KEY_CHECKS = 0;' > $dockerTempPathFile; \
  mysqldump --verbose --force -u$dockerDBUser -p$dockerDBPassword --add-drop-table --no-data $containerDB | grep ^DROP >> $dockerTempPathFile; \
  echo 'SET FOREIGN_KEY_CHECKS = 1;' >> $dockerTempPathFile; \
  mysql --verbose --force -u$dockerDBUser -p$dockerDBPassword $1 < $dockerTempPathFile && exit"
  sleep 5

  printContainerMsg "RESTORE DATABASE $1"
  docker exec $containerName bash -c "mysql --verbose -w -u$dockerDBUser -p$dockerDBPassword --force $containerDB < $dockerTempPath/$1__$currentDate.sql"
  sleep 2

  printContainerMsg "DELETE BACKUP FILE $1__$currentDate.sql FROM CONTAINER $containerName"
  docker exec $containerName bash -c "rm $dockerTempPath/$1__$currentDate.sql"
  sleep 3
}
