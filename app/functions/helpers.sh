function displaytime {
   h=`expr $1 / 3600`
   m=`expr $1  % 3600 / 60`
   s=`expr $1 % 60`
   printf "%02d:%02d:%02d\n" $h $m $s
}

function preparingFile {
  printLocalMsg 'PREPARING FILE '$1'__'$currentDate'.sql.gz'
  printLocalMsg 'STEP 1: unpacking'

  gunzip -c $localPath'/'$1'__'$currentDate'.sql.gz' > $localTempPath'/'$1'__'$currentDate'.sql'
  rm $localPath'/'$1'__'$currentDate'.sql.gz'
  sleep 1

  printLocalMsg 'STEP 2: compressing with right name'
  gzip -v -$fileCompressingLevel $localTempPath'/'$1'__'$currentDate'.sql'
  sleep 1

  printLocalMsg 'STEP 3: copy to ' $localPath
  rsync -avzP $localTempPath'/'$1'__'$currentDate'.sql.gz' $localPath'/'$1'__'$currentDate'.sql.gz'
  sleep 1

  printLocalMsg 'STEP 4: cleaning '
  rm $localTempPath'/'$1'__'$currentDate'.sql.gz'
  sleep 1

  printLocalMsg 'DONE'
  sleep 2
}
