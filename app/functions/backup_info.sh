function backup_info {
  echo '###############'
  echo '# BACKUP INFO #'
  echo '###############'
  echo

  for i in ${!remoteListDB[@]}; do

    localFilePath="$localPath/${remoteListDB[$i]}__${currentDate}.sql.gz"

    if [[ -f $localFilePath ]]; then
      echo $(tput bold)$(tput setaf 3)    $currentDate' DATABASE'$(tput sgr0) ${remoteListDB[$i]}
      echo $(tput bold)$(tput setaf 3)    $currentDate' SAVED LOCAL FILE PATH'$(tput sgr0) $localFilePath' | '"SIZE: $(du $localFilePath | awk '{print $1/1000}') MB"
      gzip -l $localFilePath
      echo $(tput bold)$(tput setaf 3)    $currentDate' TIME SPENT'$(tput sgr0) $(displaytime ${timeSpent[$i]})
      echo
    fi

  done
}
