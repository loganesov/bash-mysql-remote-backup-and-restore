function backup {

  if [[ $isNeedCreateLocalMissingDB -eq '1' ]]; then
      optionsCreateMissingDB='--databases'
  fi

  if [[ -z ${!remoteListDB[@]} ]]; then
    emptyRemoteListDB
  else
    for database in ${remoteListDB[@]}; do

      startTime="$(date +%s)"

      clear
      printMsg "### SELECT DB $database"

      echo
      printRemoteMsg 'DUMP DATABASE' $database
      ssh $remoteSSHHost 'cd '$remotePath' && mysqldump --verbose '$optionsCreateMissingDB' -u'$remoteMysqlUser' -p'$remoteMysqlPassword' '$database '>' $database'__'$sold'__'$currentDate'.sql'

      echo
      printRemoteMsg 'COMPRESSING DATABASE FILE ' $database'__'$sold'__'$currentDate'.sql'
      ssh $remoteSSHHost 'cd '$remotePath' && gzip -v -'$fileCompressingLevel $database'__'$sold'__'$currentDate'.sql'

      echo
      printLocalMsg 'FETCH DATABASE FILE' $database'__'$sold'__'$currentDate'.sql.gz'
      rsync -avzP $remoteSSHHost':'$remotePath'/'$database'__'$sold'__'$currentDate'.sql.gz' $localPath'/'$database'__'$currentDate'.sql.gz'

      echo
      printRemoteMsg 'REMOVE BACKUP FILE AFTER DUMP' $database'__'$sold'__'$currentDate'.sql.gz'
      ssh $remoteSSHHost 'cd '$remotePath' && rm '$database'__'$sold'__'$currentDate'.sql.gz'

      preparingFile $database

      if [[ $onlyDownloadBackupFiles -eq '0' && -n ${!localListDB[@]} ]]; then
        backup_restore $database
      fi

      printMsg 'DONE '$database' - '$currentDate' '"$(date +"%H:%M:%S")"
      osMessage "$database - $currentDate $(date +"%H:%M:%S") \n\n$localPath/${database}__${currentDate}.sql.gz \n\n$(du $localPath/${database}__${currentDate}.sql.gz | awk '{print $1/1000}') MB"

      timeSpent+=($(($(date +%s)-$startTime)))
    done
  fi
}
