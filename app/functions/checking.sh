function checkingAppVersion {
  echo $localVersion > version.txt

  printLocalMsg '' 'Checking App Version...'

  remoteVersion=$(curl -s -L $(curl -s -L $repositoryAPIPath'/src' | grep --color -oE 'https:\/\/api\.bitbucket\.org\/2\.0\/repositories\/loganesov\/bash-mysql-remote-backup-and-restore\/src\/[A-Za-z0-9]+\/version.txt' | tail -n 1 $1))

  if [[ (($localVersion < $remoteVersion)) ]]; then
    echo 'Has new version ' $(tput bold)$(tput setaf 2)$remoteVersion$(tput sgr0)', You`r version '$(tput bold)$(tput setaf 4)$localVersion$(tput sgr0)
    echo '--------------------------------'

    osMessage 'Has new version '$remoteVersion'\n You`r version '$localVersion

    if [[ $isCronMode -eq '0' ]]; then

      PS3="Update BackupSystem ?  "

      select optionUpdateBackupSystem in yes no; do
        case $optionUpdateBackupSystem in
        'yes')
          git pull origin master && chmod +x './run.sh' && ./run.sh;;
        'no')
          echo "Skipping update ..."
          break;;
        *)
          echo "Invalid selected option $REPLY";;
        esac
        sleep 1
      done

    fi
  fi
}