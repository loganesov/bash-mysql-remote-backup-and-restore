#!/bin/bash

folder_name=$(date '+%d_%m_%Y')

sudo mount /mnt/dav/nextcloud_home/

sudo mkdir '/mnt/dav/nextcloud_home/Backup/3atdev/'$folder_name

sudo rsync -azvP --exclude={'.gitignore','remoteDBlist.txt'} --remove-source-files $(pwd)/backup/ '/mnt/dav/nextcloud_home/Backup/3atdev/'$folder_name'/'

sudo umount /mnt/dav/nextcloud_home/