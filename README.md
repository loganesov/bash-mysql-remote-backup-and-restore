## v2.4

--- 

## Change List
###### 25.12.2020 v2.4
Added checking for program Dialog to show Menu for select DB fetching 
Updated README.md

###### 24.12.2020 v2.3
Added Notify from OS
Updated README.md

See more in `change_list.txt` 

---

## Usage

---
0. **!!!** If you pulled new changes from the master,
   then make sure your **config.env** file matches the parameters with **confing_sample.env** 
1. Clone a repository
2. Copy file **config_sample.env** to **config.env**
3. In **config.env** you need fill parameters
- `remoteSSHHost` 
- `remoteMysqlUser` 
- `remoteMysqlPassword` 
- `localMysqlUser`
- `localMysqlPassword`
- `localListDB (List of local databases available)`
4. After fill config.env, you need **./run.sh**

---

**Example for localListDB**

localListDB=('local-test' 'local-test-2' 'goolge-shop')

---

## For Cron OR Permanent run without choice of databases
1. Fill list - **remoteListDB**
2. Fill list - **localListDB**
3. Set **'0'** for - **isManualSelectionDB** 
3. Set **'1'** for - **isCronMode** 
4. Add **./run.sh** to cronelist

---

## OPTIONS

---

### remoteListDBFilter 
Gets a list of remote databases and filters out only those databases that are mentioned in the list

### remoteListDB
If the array is full, then databases from this list are downloaded from the server  

### localListDB 
List of Array, local databases available, must be filled in quotes and separated by a space.  

### isManualSelectionDB
Show Menu with remote db list for choice
- 1 - show menu
- 0 - skip menu

### onlyDownloadBackupFiles
- 1 - skip backup restore
- 0 - backup restore

### showBackupInfo
- 1 - show
- 0 - hide

### isNeedCreateLocalMissingDB
- 1 - create missing local database if not exists
- 0 - skip create missing local database


### fileCompressingLevel
- All sql files compressing with gzip
- 6 - default
- 1 - 9 Level

### isDockerSystem
- 1 - restore in docker container for service
- 0 - restore in global system mysql

### dockerContainerPrefix

### dockerContainerPostfix

### dockerDBPrefix

### dockerDBPostfix

### dockerDBUser

### dockerDBPassword

For docker containers, you must fill in Mysql Login and Password, all docker containers must have such a user.
